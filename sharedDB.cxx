#include <stdio.h>
#include <omp.h>
#include <sqlite3.h>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <unistd.h>

inline std::string quotesql( const char c ) {
    return std::string("'") + c + std::string("'");
}
inline std::string quotesql( const std::string s ) {
    return std::string("'") + s + std::string("'");
}



int main() {
	sqlite3 *db = NULL;
	char* error_msg = NULL;

	if ( SQLITE_OK != sqlite3_open_v2("file::memory:?cache=shared", &db, SQLITE_OPEN_URI | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL))
		return -1;

	/* Create SQL statement */
	std::string sql = "create table aTable( "
			"a TEXT, "
			"b TEXT, "
			"PRIMARY KEY (a) );";

	/* Execute SQL statement */
	int sqlite_rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
	if ( SQLITE_OK == sqlite_rc) {
		omp_set_num_threads(6);

		const char *chars = "abcdefghijk";

		int tid=-1;
#pragma omp parallel
		{
			unsigned int i = 0;
#pragma omp for private(i,tid)
			for (i=0; i < sizeof(chars); i++) {
				 tid = omp_get_thread_num();
				  printf("Hello World from thread = %d\n", tid);

				sqlite3 *db_shared = NULL;
				if ( SQLITE_OK
						!= sqlite3_open_v2("file::memory:?cache=shared",
								&db_shared, SQLITE_OPEN_URI | SQLITE_OPEN_READWRITE, NULL )) {
					std::cerr << "open same in memory DB not possible"
							<< std::endl;
#pragma omp cancel for

				}

				std::stringstream sql_statement;
				sql_statement
						<< "INSERT OR REPLACE INTO aTable (a, b) VALUES ("
						<< quotesql(chars[i]) << ", "
						<< quotesql("test") << " ) ";
				if ( SQLITE_OK
						!= sqlite3_exec(db_shared, sql_statement.str().c_str(),
						NULL, 0, &error_msg)) {
					std::cerr << "failed to insert data with statement"
							<< std::endl << "\t" << sql_statement.str()
							<< std::endl << error_msg << std::endl;
					sqlite3_free(error_msg);
					error_msg = NULL;
				}
				(void) sqlite3_close(db_shared);
			}
		}
#pragma omp barrier
		{  //write in memory DB to disk
			sqlite3 *to_db=NULL;
			const std::string targetDB("./test.db");
			(void)unlink(targetDB.c_str());
		    int err = sqlite3_open_v2(targetDB.c_str(), &to_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
		    if( SQLITE_OK != err ) {
		        std::cerr << "failed to create target database " << sqlite3_errstr(err);
		        return -2;
		    }
		    sqlite3_backup *backup = sqlite3_backup_init(to_db, "main", db, "main");
		    if(backup){
		        (void)sqlite3_backup_step(backup, -1);
		        (void)sqlite3_backup_finish(backup);
		    }
		    err = sqlite3_errcode(to_db);
		    if( SQLITE_OK !=  err ){
		        std::cerr << sqlite3_errstr(err);
		    }
		    (void)sqlite3_close(to_db);
		}
	} else {
		if (NULL != error_msg) std::cerr << error_msg << std::endl;
		(void) sqlite3_close(db);
		db = NULL;
	}

	return 0;
}


